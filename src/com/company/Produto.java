package com.company;

import java.sql.SQLOutput;

public class Produto {
    private String nome;
    private String descricao;
    private static double rendimento = 0.007;

    public Produto() {}

    public Produto(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public static double calcularAcumulado(double capital, double quantidadeMeses) {
       return Math.pow((1 + rendimento), quantidadeMeses) * capital;
    }
}
