package com.company;

public class Cliente {
    private int id;
    private String nome;
    private int idade;
    private double capital;

    public Cliente() {}

    public Cliente(int id, String nome, int idade, double capital) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
        this.capital = capital;
    }

    public String getNome() {
        return nome;
    }

    public double getCapital() {
        return capital;
    }
}
