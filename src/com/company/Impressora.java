package com.company;

import java.util.Scanner;

public class Impressora {
    private Scanner leitura = new Scanner(System.in);

    public Impressora init() {

        System.out.println("Ola! Gostaria de realizar uma simulação de um produto de investimento? ");
        System.out.println("Qual seu nome?");
        String nome = leitura.next();

        System.out.println("Qual sua idade?");
        int idade = leitura.nextInt();

        System.out.println("Digite o CAPITAL a ser investido:");
        int capital = leitura.nextInt();

        System.out.println("E qual a quantidade de meses que deseja:");
        int quantidadeMeses = leitura.nextInt();

        Cliente cliente = new Cliente(2, nome, idade, capital);
        Produto produto = new Produto("Produto XPTO", "Lorem ipsum dolar");

        double valorAcumulado = produto.calcularAcumulado(capital, quantidadeMeses);

        System.out.println(cliente.getNome() + " o valor acumulado de R$ " + cliente.getCapital()
                + " em " + quantidadeMeses + " meses será de ----> " + valorAcumulado + " utilizando o produto " + produto.getNome());

        return null;
    }

}
